from django.db import models as mod
import threading
import numpy as np
import cv2
import os
from tensorflow.keras import models
import tensorflow as tf

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.compat.v1.Session(config=config)

class Genre(mod.Model):
    """docstring for Genre"""
    genre = mod.CharField(max_length=100)
    emotion = mod.CharField(max_length=100)

class PlayList(mod.Model):
    """docstring for PlayList"""
    user_id = mod.IntegerField(default=0)
    name = mod.CharField(max_length=200)

class ListSongs(mod.Model):
    """docstring for ListSongs"""
    list_id = mod.IntegerField(default=0)
    song_name = mod.CharField(max_length=200)
    artist_name = mod.CharField(max_length=200)
    genre = mod.CharField(max_length=100)
    link = mod.CharField(max_length=500)

class emotion(mod.Model):
    """docstring for emotion"""
    emotion = mod.CharField(max_length=20)
    user_id = mod.IntegerField(default=0)
    date = mod.DateField(auto_now=False)

# Create your models here.
class VideoCamera(object):
    emotion_dict = {0: "Angry", 1: "Disgust", 2: "Fear", 3: "Happy", 4: "Sad", 5: "Surprise", 6: "Neutral"}
    prediction = ""
    emotion = ""
    model = models.load_model('/Users/hoshinorin/PycharmProjects/pythonProject/face/detect/models/my_model1.h5')
    face_cascade = cv2.CascadeClassifier("/Users/hoshinorin/PycharmProjects/pythonProject/face/detect/models/cascade.xml")
    path = '/Users/hoshinorin/PycharmProjects/pythonProject/face/detect/static/'
    p = "/Users/hoshinorin/PycharmProjects/pythonProject/face/detect/static/Neutral.png"

    def __init__(self):
        self.video = cv2.VideoCapture(0 + cv2.CAP_DSHOW)
        (self.grabbed, self.frame) = self.video.read()
        threading.Thread(target=self.update, args=()).start()

    def __del__(self):
        self.video.release()

    def stop(self):
        self.video.release()

    def get_frame(self, state):
        if state == True:
            image = self.frame
            if self.grabbed:
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                faces = self.face_cascade.detectMultiScale(gray, 1.1, 4)
                for (x, y, w, h) in faces:
                    cv2.rectangle(image, (x, y), (x + w, y + h), (209, 232, 226), 2)
                    roi_gray = gray[y:y + h, x:x + w]
                    cropped_img = np.expand_dims(np.expand_dims(cv2.resize(roi_gray, (48, 48)), -1), 0)
                    cv2.normalize(cropped_img, cropped_img, alpha=0, beta=1, norm_type=cv2.NORM_L2, dtype=cv2.CV_32F)
                    self.prediction = self.model.predict(cropped_img)
                    self.emotion = self.emotion_dict[int(np.argmax(self.prediction))]
                    cv2.putText(image, self.emotion, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (209, 232, 226), 1,
                                cv2.LINE_AA)
                _, jpeg = cv2.imencode('.jpg', image)
                self.prev = jpeg.tobytes()

                return self.prev
            else:
                return self.prev

    def update(self):
        while True:
            (self.grabbed, self.frame) = self.video.read()


class VideoCamera1(object):
    emotion_dict = {0: "Angry", 1: "Disgust", 2: "Fear", 3: "Happy", 4: "Sad", 5: "Surprise", 6: "Neutral"}
    prediction = ""
    emotion = ""
    model = models.load_model('/Users/hoshinorin/PycharmProjects/pythonProject/face/detect/models/my_model1.h5')
    face_cascade = cv2.CascadeClassifier("/Users/hoshinorin/PycharmProjects/pythonProject/face/detect/models/cascade.xml")
    path = '/Users/hoshinorin/PycharmProjects/pythonProject/face/detect/static'
    p = "/Users/hoshinorin/PycharmProjects/pythonProject/face/detect/static/Neutral.png"

    def __init__(self):
        self.video = cv2.VideoCapture(0 + cv2.CAP_DSHOW)
        (self.grabbed, self.frame) = self.video.read()
        threading.Thread(target=self.update, args=()).start()

    def __del__(self):
        self.video.release()

    def stop(self):
        self.video.release()

    def get_frame(self, state):
        while state == True:
            image = self.frame
            if self.grabbed:
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                faces = self.face_cascade.detectMultiScale(gray, 1.1, 4)
                for (x, y, w, h) in faces:
                    cv2.rectangle(image, (x, y), (x + w, y + h), (209, 232, 226), 2)
                    roi_gray = gray[y:y + h, x:x + w]
                    cropped_img = np.expand_dims(np.expand_dims(cv2.resize(roi_gray, (48, 48)), -1), 0)
                    cv2.normalize(cropped_img, cropped_img, alpha=0, beta=1, norm_type=cv2.NORM_L2, dtype=cv2.CV_32F)
                    self.prediction = self.model.predict(cropped_img)
                    self.emotion = self.emotion_dict[int(np.argmax(self.prediction))]
                    cv2.putText(image, self.emotion, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (209, 232, 226), 1,
                                cv2.LINE_AA)
                _, jpeg = cv2.imencode('.jpg', image)
                self.prev = jpeg.tobytes()
                self.p = self.path + self.emotion + ".png"
                cv2.imwrite(self.p, image)
                state = False
            else:
                state = False
        # else:
        self.stop()
        return self.p

    def update(self):
        while True:
            (self.grabbed, self.frame) = self.video.read()