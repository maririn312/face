import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
from .models import Genre
from django.db import connection

cid = 'b3da5f16ddb246e4bec6964d9b8e8bfa'
secret = '92b28ead977d4ed287b53d5a77d0e5c2'
client_credentials_manager = SpotifyClientCredentials(client_id=cid, client_secret=secret)
sp = spotipy.Spotify(client_credentials_manager = client_credentials_manager)

class Music(object):
	"""docstring for Music"""
	def __init__(self, emotion):
		self.genre1 = []
		cursor = connection.cursor()
		# cursor.execute('SELECT genre FROM detect_genre WHERE emotion = `emotion`')
		cursor.execute("SELECT DISTINCT genre FROM detect_genre WHERE emotion = %s", (emotion))
		#(cursor.fetchall())
		for g in cursor.fetchall():
			g = ' '.join(g)
			print(g)
			self.genre1.append(g)
		self.artist_name = []
		self.track_name = []
		self.album = []
		self.genre = []
		for x in self.genre1:
			for i in range(3):
				gen = 'genre:'+x
				track_results = sp.search(q=gen, type='track', limit=1, offset=i)
				for i, t in enumerate(track_results['tracks']['items']):
					t = track_results['tracks']['items'][0]
					self.artist_name.append(t['artists'][0]['name'])
					self.track_name.append(t['name'])
					self.album.append(t['album']['external_urls']['spotify'])
					# for x in self.album['genres']:
					self.genre.append(x)

				

		# import pandas as pd
		# track_dataframe = pd.DataFrame({'artist_name' : artist_name, 'track_name' : track_name, 'track_id' : track_id, 'popularity' : popularity})
		# print(track_dataframe.shape)
		# track_dataframe.head()
		

def get_Music(emotion):
	return Music(emotion)
	pass

# def make_list(d):
#     def append_children(parent, d):
#         children = [[x['Child']] for x in d if x['Parent'] == parent[0]]
#         if children:
#             parent.append(children)
#             for child in children:
#                 append_children(child, d)

#     results = [[x['Child']] for x in d if x['Parent'] == 'top']
#     for parent in results:
#         append_children(parent, d)

#     return results

# t = getMusic()

# print(t.artist_name[0])
# print(t.track_name[0])
# print(t.album[0])
# print(t.genre[0])

